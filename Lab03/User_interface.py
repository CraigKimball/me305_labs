'''
@file User_interface.py
@image html UI_3.png width=1000cm
@brief This file is a FSM used to run the Encoder User Interface

@detials Depending on the input from the user this FSM will send the Command to the 
Lab03 Module which will grab encoder data and send it back to be read by the user
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab03/User_interface.py
'''

import shares
from encoder import Encoder
import pyb
from pyb import UART
import utime

class TaskUI:
    '''
    @brief      A finite state machine to control a Blinking LED
    @details    This class implements a finite state machine to control the
                operation of LED na dthe type of LED being used
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_CHECK_INPUT_STATE       = 1
    
    ## Constant defining State 2
    S2_OUTPUT       = 2
    
 
    
    def __init__(self, interval):
        '''
        @brief      Creates a UI object.
   
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## A class attribute "copy" of the LED
        # Stores class copy of LED so other functions can
                           # use the LED object
        
        ## A class attribute "copy" of the LED_Type object
       
        ## assiging t2ch1 to the LED hardware
        self.uart = UART(2)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteration of the Encoder checking its own value
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Press z to zero Encoder position')
                print('Press p to print Encoder position')
                print('Press d to print current Encoder Delta')
                self.transitionTo(self.S1_CHECK_INPUT_STATE)
                #print(str(self.runs) + ': State 0 ' + str(utime_ticks_diff(self.curr_time,self.start_time)))
                
                
             elif(self.state == self.S1_CHECK_INPUT_STATE):
                if(self.uart.any()):
                    print('input received')
                    shares.cmd = self.uart.readchar()
                    self.transitionTo(self.S2_OUTPUT)
                
             elif(self.state == self.S2_OUTPUT):
                 if shares.resp:
                     self.transitionTo(self.S1_CHECK_INPUT_STATE)
                     self.uart.writechar(shares.resp)
                     shares.resp = None
     
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
             self.runs += 1
             self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

        


        
    

