'''
@file Lab03.py
@image html EncoderFSM.png width=1000cm
@brief This file is a FSM used Track Encoder movement and implement 

@detials This FSM reads the character input from the user and based on that input will output
information regarding the encoder, and then send that info back to the User.
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab03/Lab03.py
'''

import shares
from encoder import Encoder
import pyb
from pyb import UART
import utime

class TaskEncoder:
    '''
    @brief      A finite state machine to control a Blinking LED
    @details    This class implements a finite state machine to control the
                operation of LED na dthe type of LED being used
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_UPDATE_ENCODER_POSITION       = 1
    
    ## Constant defining State 2
    S2_UPDATE_ENCODER_POSITION       = 2
    
 
    
    def __init__(self, interval, Encoder_track):
        '''
        @brief      Creates a Encoder object.
        @param Encoder_track. Tracks and stores information from the encoder based on
        functions from the encoder module.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        self.Encoder_track = Encoder_track
        ## Variable that recieves and stores encoder information
        

        
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteration of the Encoder checking its own value
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.Encoder_track.zero()
                self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                
                
             elif(self.state == self.S1_UPDATE_ENCODER_POSITION):
                self.Encoder_track.update()
                if(shares.cmd == 122):
                    shares.resp =  self.Encoder_track.zero()
                    print(shares.resp)
                    self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                    shares.cmd = None
                if(shares.cmd == 112):
                    shares.resp = self.Encoder_track.get_position()
                    print(shares.resp)
                    self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                    shares.cmd = None
                if(shares.cmd == 100):
                    shares.resp = self.Encoder_track.get_delta()
                    print(shares.resp)
                    self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                    shares.cmd = None
                else:
                    self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                
              
             
                    
                
        else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
        self.runs += 1
        self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

        


        
    

