## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Craig Kimball
#
#  @copyright License Info
#
#  @date 10/13/2020
#
#  @package encoder
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Craig Kimball
#
#  @copyright License Info
#
#  @date 10/13/20
# Source Code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab03/encoder.py
import pyb

## An encoder driver object
#
#  Details
#  @author Criag Kimball
#  @copyright License Info
#  @date 10/13/20
class Encoder:

	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
    def __init__(self):
        # User Inputs for setting up timing channel and used pins by encoder
        
        #initializing the Encoder readout using User inputs
        
        self.tim = pyb.Timer(4)
        self.tim.init(prescaler=0,period=65535)
        self.tim.channel(1,pin=pyb.Pin.cpu.B6, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2,pin=pyb.Pin.cpu.B7, mode=pyb.Timer.ENC_AB)
        self.Last_val = 0




    ## Updates a variable that just stores and displays the current position of the encoder
    #
    # This funtion takes the current position of the encoder and corrects for over or underflow
    # before adding the change to a total elapsed position value
    def update(self):
        self.Last_val = self.Current_val
        self.Current_val = self.tim.counter()
        
        
        self.delta_check = self.Current_val - self.Last_val
        if(self.delta_check > 32767.5):
            self.delta_good = self.delta_check - 65535
            #Converting Ticks to deg
            self.delta_good = self.delta_good / 3999.6
            
            # Sent position in degrees
            self.position = self.position + self.delta_good
        
        elif(self.delta_check < -32767.5):
            self.delta_good = self.delta_check + 65535
            #Converting Ticks to deg
            self.delta_good = self.delta_good / 3999.6
            
            # Sent position in degrees
            self.position = self.position + self.delta_good
        else:
            self.delta_good = self.delta_check
            #Converting Ticks to deg
            self.delta_good = self.delta_good / 3999.6
            
            # Sent position in degrees
            self.position = self.position + self.delta_good
            
        return(self.position)

    
    ## Gets the encoder's position
    #
    #
    def get_position(self):
        return(self.position)


    ## Takes the last stored value of the encoder position and finds difference to current position
    #
    #
    def get_delta(self):
        return((self.delta_good) * -1)
    
    ## Sets the position of the encoder at 0
    #
    #
    def zero(self):
        self.tim.counter(0)
        self.Current_val = 0
        self.position = 0
        return(self.position)
