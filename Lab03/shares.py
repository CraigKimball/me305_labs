'''
@file shares.py
@brief A container for all the inter-task variables
@ details Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab03/shares.py
@author Charlie Refvem implemented by Craig Kimball
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None