'''
@file Lab02.py

@brief This file is a FSM used to run two LEDs on the nucleo board cooperatively

@detials Depending on the input of 'LED_Type' parameter this FSM will either exectue a triangle
wave pattern for the LED to follow which will use the built in LD2 on hardware, or a binary virtual
LED that will simply print whether it is on or off.
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab02/Lab02.py
'''

import pyb
import utime

class TaskBlinky:
    '''
    @brief      A finite state machine to control a Blinking LED
    @details    This class implements a finite state machine to control the
                operation of LED na dthe type of LED being used
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_PRINT_OFF                    = 1
    
    ## Constant defining State 2
    S2_PRINT_ON                     = 2
    
 
    
    def __init__(self, interval, LED, LED_value, LED_Type):
        '''
        @brief      Creates a LED object.
        @param LED an object from class LED representing THE Physical and virtual LED
        @param LED_value this variable is used to store what current % power the LED should be
        @param LED_Type Tells the task whether the LED is Triangle or Binary
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        self.LED = LED
        ## A class attribute "copy" of the LED
        # Stores class copy of LED so other functions can
                           # use the LED object
        self.LED_value = 0
        
        self.LED_Type = LED_Type
        ## A class attribute "copy" of the LED_Type object
       
        ## assiging t2ch1 to the LED hardware
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2 , freq = 20000)
        self.t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteratoin of Time passing and the LED operating
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                
              

                self.LED.Stop()
                self.transitionTo(self.S1_PRINT_OFF)
                self.LED_value = 0
               # print(str(self.runs) + ': State 0 ' + str(utime_ticks_diff(self.curr_time,self.start_time)))
                
                
             elif(self.state == self.S1_PRINT_OFF):
              #  print(str(self.runs) + ' State 1 {:0.2f}'.format(utime_ticks_diff(self.curr_time,self.start_time)))
                # Checking whether it is operating as the physical or virtual LED
                if(self.LED_Type == 0):
                    if(self.LED_value < 100):
                        self.LED_value+= 10

                        self.t2ch1.pulse_width_percent(self.LED_value)
                        #print('up 10')
                        #print(str(self.LED_value))
                        self.transitionTo(self.S2_PRINT_ON)
                    else:
                        self.LED_value = 0

                        self.t2ch1.pulse_width_percent(self.LED_value)
                        #print('Reset')
                        #print(str(self.LED_value))
                        self.transitionTo(self.S2_PRINT_ON)
                elif(self.LED_Type == 1):
                    self.LED.Start()
                    self.transitionTo(self.S2_PRINT_ON)
                else:
                    pass
                
               
             elif(self.state == self.S2_PRINT_ON):
                #print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Checking to see if First Floor Button Has been pressed 
                
                if(self.LED_Type == 0):
                    if(self.LED_value < 100):
                        self.LED_value+= 10

                        self.t2ch1.pulse_width_percent(self.LED_value)
                        #print('up 10')
                        #print(str(self.LED_value))
                        self.transitionTo(self.S1_PRINT_OFF)
                    else:
                        self.LED_value = 0
                        self.t2ch1.pulse_width_percent(self.LED_value)
                        #print('Reset')
                        #print(str(self.LED_value))
                        self.transitionTo(self.S1_PRINT_OFF)
                elif(self.LED_Type == 1):
                    self.LED.Stop()
                    self.transitionTo(self.S1_PRINT_OFF)
                else:
                    pass
                
            
                    # Motor is off
                    
                
            
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
             self.runs += 1
             self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
     
        
    
 


class LED_State:
    '''
    @brief      A LED_State tracker for the binary LED
    @details    This class represents a virtual binary LED turning on and off
                
    '''
    
    def __init__(self):
        
        '''
    
        @brief Creates a LED Object
        '''
        pass
    
    
  
    def Start(self):
        '''
        @brief Turns LED on
        '''
        print('LED_on')
    
    
    def Stop(self):
        '''
        @brief Turns LED off
        '''
        print('LED_off')

        
    

