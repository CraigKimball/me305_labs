'''
@file main_hw0x0.py
@image html hw_0_FSM.png width=1000cm
@brief This is the main task program for running the elevator FSM

This program allows for use to operate multiple instances of the FSM. In the
 current state it is running to instances of the elevator
at the same time. Source code:https://bitbucket.org/CraigKimball/me305_labs/src/master/Homework/main_hw0x0.py

'''

## @image html hw_0_FSM.png

from lab02_local import TaskBlinky, LED_State

## Motor Object
LED = LED_State()
LED_Value = 0
Triangle = 0
Binary =1
## Task object
task1 = TaskBlinky(0.1, LED, LED_Value, Triangle)
task2 = TaskBlinky(0.1, LED, LED_Value, Binary)# Will also run constructor

# To run the task call task1.run() over and over
for N in range(100000000000): #Will change to   "while True:" once we're on hardware
#    task1.run()
    task2.run()
#    task3.run()
