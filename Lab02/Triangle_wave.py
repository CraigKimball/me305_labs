# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:29:06 2020

@author: craig
"""

import pyb


pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2 , freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
LED_VAL = 0

while(LED_VAL < 100):
    LED_VAL+=10
    t2ch1.pulse_width_percent(LED_VAL)
    pyb.delay(50)
    print('LED up 10')
else:
    LED_VAL = 0
    t2ch1.pulse_width_percent(LED_VAL)
    print('LED now off')