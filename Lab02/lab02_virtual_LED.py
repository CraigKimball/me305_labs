'''
@file Hw0x0.py

@brief This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator

@detials The user has a button to go to floor one or floor 2.
There is also a limit switch at each floor to tell the elevator when to stop.
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Homework/Hw0x0.py
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control a two story elevator.
    @details    This class implements a finite state machine to control the
                operation of a two story elevator.
    '''
    
    ## Constant defining State 0 
    S0_INIT            = 0
    
    ## Constant defining State 1
    S1_PRINT_OFF                    = 1
    
    ## Constant defining State 2
    S2_PRINT_ON          = 2
    
 
    
    def __init__(self, interval, LED):
        '''
        @brief      Creates a Elevator object.
        @param Button_1   An object from class Button representing floor 1 button
        @param Button_2  An object from class Button representing floor 2 button
        @param First     An object from class Button representing Floor 1 sensor
        @param Second    An object from class Button representing Floor 2 sensor
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.LED = LED # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the Button_1 object
       
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteratoin of checking the elevator position and selecting an input to move states
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_PRINT_OFF)
                self.LED.Stop()
                self.Led_state = 0
           
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                
                
             elif(self.state == self.S1_PRINT_OFF):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Checking to see if First Floor Button Has been pressed 
                self.LED.Stop()
                self.Led_state = 0
                self.transitionTo(self.S2_PRINT_ON)
                
                '''
                if(self.Button_2.getButtonState()):
                    self.Button_2_state = 1
                    print('button 2 pushed')
                    # Sets Button_2 state to pushed
                    self.transitionTo(self.S1_MOVING_UP)
                    self.Motor.Up()
                    # Motor is raising the elevator
                if(self.Button_1.getButtonState()):
                    self.Button_1_state = 0
                    #Does not let button 1 become active even if it is pushed
                    self.transitionTo(self.S0_STOPPED_ON_FLOOR_1)
                '''
             elif(self.state == self.S2_PRINT_ON):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Checking to see if First Floor Button Has been pressed 
                self.LED.Start()
                self.Led_state = 1
                self.transitionTo(self.S1_PRINT_OFF)
                
            
                    # Motor is off
                    
                
            
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
             self.runs += 1
             self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
 
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents an imaginary button used to act as the 
    elevator buttons and limit switches within the elevator FSM. This button
    class will return a random boolean statement when checked to attempt to 
    model a human interacting with the elevator.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True,False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to raise and lower 
                the elevator
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the elevator up
        '''
        print('Elevator going up')
    
    def Start(self):
        '''
        @brief Moves the elevator down
        '''
        print('LED_on')
    
    def Stop(self):
        '''
        @brief Moves the motor Stops the elevator
        '''
        print('LED_off')

        
    

