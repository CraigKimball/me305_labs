'''
@file main_lab03.py

@brief This is the main task program for running the Encoder and UI cooperatively

This program tracks the total elapsed position of an encoder 
and will print information about the encoder to the user when prompted. The interval for this task
must be quick enough such that the 16 bit encoder does not travel through and over its range in one iteration of the FSM
Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab04/main_UI.py

'''

## @image html hw_0_FSM.png

from User_interface_lab04_ky import TaskUI

## Encoder Object
## Task object
task1 = TaskUI(.1)


# To run the task call task1.run() over and over
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
#    task3.run()
