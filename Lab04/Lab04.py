'''
@file Lab04.py

@brief This file is a FSM used Track Encoder movement and send to the computer to plot

@detials This FSM reads the character input from the user and based on that input will output
information regarding the encoder, and then send that info back to the User for plotting position vs time
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab04/Lab04.py
'''



from pyb import UART
import utime

from encoder import Encoder

class TaskEncoder:
    '''
    @brief      A finite state machine to store Encoder and Time data
    @details    This class implements a finite state machine to control store current position
    and time in a list as time passes to send to a computer
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_UPDATE_ENCODER_POSITION       = 1
    
    ## Constant defining State 2
    S2_SENDING_DATA      = 2
    
    ## State used for debugging. Think of like a hold command
    S3_NULL               = 3
    
 
    
    def __init__(self, interval, Encoder_track):
        '''
        @brief      Creates a Encoder object.
        @param Encoder_track. Tracks and stores information from the encoder based on
        functions from the encoder module.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        self.Encoder_track = Encoder_track
        ## Variable that recieves and stores encoder information
        
        ## setting up UART conection
        self.uart = UART(2)
        
        ## Two lists. First one will store Encoder position, second will store time
        self.enc_val = []
        self.time = []
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteration of the Encoder checking its own value
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                #Creates empty arrays for both time and encoder_value
                self.U_input = 0
                self.Encoder_track.zero()
                
                self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                
                
             elif(self.state == self.S1_UPDATE_ENCODER_POSITION):
                self.Encoder_track.update()
                #Checking to see if command has been sent to board
                if(self.uart.any() != 0):
                    self.U_input = self.uart.readchar()
                    
                
                # If g is the key pressed then values will start to be recorded
                elif(self.U_input == 103):
                    self.enc_val.append(self.Encoder_track.get_position())
                    self.time.append(self.next_time)

                    self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
               
                # transitioning to data printing for UI if s is received
                elif(self.U_input == 115):
                    self.transitionTo(self.S2_SENDING_DATA)
                    self.U_input = 0

             # state where data is sent from the Nucleo back to the PC via REPL
             elif(self.state == self.S2_SENDING_DATA):
                 
                 #For loop that prints the length of the lists in CSV format
                 
                 for n in range (len(self.enc_val)):
                     print('{:},{:}'.format(self.time[n],self.enc_val[n]))
                 print('END')
                 self.transitionTo(self.S1_UPDATE_ENCODER_POSITION)
                    
                    
                
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
             self.runs += 1
             self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

     
        
    

