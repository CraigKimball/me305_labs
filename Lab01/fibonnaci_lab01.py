## @file fibonnaci_lab01.py
'''
Created on Tue Sep 22 08:40:23 2020
@file    fibonnaci_lab01.py
@brief   This is a fibonacci counter program. To see the source code for this program refer to the following link
https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab01/fibonnaci_lab01.py
@details This program takes a user input number/ integer and returns the fibonacci number
of the corresponding sequence. It will also validate the user input and display an error
if the user inputs an invalid input type. 
@author  craig
'''

''' @fibonnaci_lab01.py
This program takes a user input number/ integer and returns the fibonacci number
of the corresponding sequence. It will also validate the user input and display an error
if the user inputs an invalid input type. '''

def fib(idx):
    '''
    @brief   This is a function that takes a users input and spits out the index fibonacci number
    @details  First the function makes sure the number is a whole number greater than zero.
              Once that is confirmed the function goes through and uses a formula to calculate the fibonacci output
    '''
    f0 = 0
    f1 = 1
    if idx <= 0:
            print('You must input a number greater than 0')
    elif idx == 1:
        #return 1 # This elif is used because our fibonacci function does not work for int 1 and 2
        print(f1)
   # elif ValueError(int):
      #  print('you must enter a whole number')
    else:
        for i in range(2,idx):
            fn = f0 + f1
            f0 = f1
            f1 = fn
        print(f1)
    print ('calculation Fibonacci number at index n = {:}.'.format(idx))
    

if __name__ == '__main__':
    user_input = int(input('Please enter a value greater than 0:'))
    fib(user_input)