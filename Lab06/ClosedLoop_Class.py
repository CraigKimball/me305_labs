
"""
@file Lab06.py
@brief This module calculates error between reference and actual motor velocity and calculates necessary acuation value to correct
@details this Controller is what feeds an acuation value to the motor in our closed loop system.
By giving it a proportional gain and a reference velocity it can pull the current motors velocuty and find the error between
measured and actuall. With this error found the controller will then set the motor to a new acuation value to try to correct this error
Source Code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/ClosedLoop_Class.py
"""
import pyb



class ClosedLoop:
    """This class implements the encoder and motor driver to control a motor and track velocity"""
    
    def __init__(self):
        """
        @param Prop_gain An input used to set the proportional gain for the Controller
        """
        self.Omega_meas = 0 
        self.Omega_ref  = 0
        
    ## Computes and returns acuation value of the motor based on measured and reference
    # values. The acuation value is referred to as L in equations
    def LoopRun(self, Omega_ref, gain, omega_measured):
        
        # Units of K'p are %/(rad/s) this is will be passed from the Back end task
        self.Prop_gain = gain
        acuation_sig = self.Prop_gain * (Omega_ref - self.Omega_meas)
        return(acuation_sig)
    
    ## Prints the currently used gain value
    def get_Kp(self):
        print(self.Prop_gain)
        
    ## Asks for an new input gain and replace current gain with new value    
    def set_Kp(self):
        
        self.Prop_gain_desired = input("input desired gain value")
        self.Prop_gain = self.Prop_gain_desired
        print('Gain value set to' + str(self.Prop_gain))
        