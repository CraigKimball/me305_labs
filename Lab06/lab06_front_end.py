'''
@file lab06_front_end.py

@brief This file is a FSM used to hadle PC user interface and allowing user to enter
a kp value for use

@details This class implements a finite state machine to control the
user inputs and output the response plots of the motor velocity
Source Link:https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/lab06_front_end.py
'''
import numpy as np
import matplotlib.pyplot as plt
import time
import serial
import keyboard

Velocity = []
Time     = []
converted = None
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
## A counter showing the number of times the task has run



K_p = input('Please ener a proportional gain value follwoing the format: "X.X" ')
K_p = float(K_p)
ser.write('{:}\r\n'.format(K_p).encode())

# Sleep the program for 6 seconds. This is longer than the time 
# it will take  for the program to make the reponse arrays
time.sleep(20)

 
 #creates a variable that holds the raw REPL output from nucleo with /n


# Creates plotting arrays until END message is read
while(converted != 'END'):
    raw_data = ser.readline().decode('ascii')
 #Takes raw data and removes the /n from the string
    converted = raw_data.strip()
 #creates a temporary list that holds the time and Encoder pos value split from string into a list
    temp_list = converted.split(',')
    
 # appends temporary list values to local lists as floating point 
    if(temp_list[0] != 'END'):
        print(temp_list)
        Time.append(float(temp_list[0])/1e6)
        Velocity.append(float(temp_list[1]))

ser.close()

     

#Making plotted lists into exportable array
data1 = np.array(Velocity)
data2 = np.array(Time)
#saving array as a CSV file
np.savetxt('Velocity.csv',data1,delimiter=',')
np.savetxt('Time.csv',data2,delimiter=',')
 
#Plotting the Encoder Position and Time Lists
plt.plot(Time,Velocity)
plt.xlabel('Time (sec)')
plt.ylabel('Velocity deg/sec')
plt.show()


    

