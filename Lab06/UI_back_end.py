'''
@file UI_back_end.py
@image html Lab06_back_fsm.png width=800cm
@brief This file is a FSM used to receive data from the user and send it to the controller.
It also sets the Omega reference for the controller block to compare for error

@details This FSM reads reads an input from the user, specifically a Kp value.
This value is then passed to the Controller along with a value from a step input list.
As the FSM iterates the task will send the next value in a list defining the step input such
that the controler can adjust acuation value. This data is from the controller is then 
sent back to this module where it is stored in another list and then sent to the Front end.
Source Link: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/UI_back_end.py
'''



from pyb import UART
import utime
import shares
import pyb
from encoder import Encoder

class BackEnd:
    '''
    @brief      A finite state machine to stores a Step input, and data lists about motor velocity
    @details    This class implements a finite state machine to pass a step input to our motor controller task
    and receives data about measured velocity from the motor to be stored and sent to the UI front end
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_PASSING_DATA      = 1
    
    ## Constant defining State 2
    S2_SENDING_DATA      = 2
    
    ## State used for debugging. Think of like a hold command
    S3_NULL               = 3
    
 
    
    def __init__(self, interval, ):
        '''
        @brief      Creates a Encoder object.
        @param Interval. This sets the interval at which the FSM will iterate on itself
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Variable that recieves and stores encoder information
        
        ## setting up UART conection
        self.uart = UART(2)
        
        ## Two lists. First one will store Omega_measured of output shaft, second will store time
        self.Velocity = []
        self.time = []
        ## Creating an object to store the input K_p value
        self.K_p = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteration of sending step input data to the controller
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                #Zeroing out global variables and variabels
                self.U_input = 0
                # this variable will be used to check what value of the step list is read next
                self.step_count = 0
                shares.resp = None
                shares.resp1 = None
                self.transitionTo(self.S1_PASSING_DATA)
                
                
             elif(self.state == self.S1_PASSING_DATA):
                #Checking to see if command has been sent to board
                
                if(self.uart.any() != 0):
                    self.K_p = self.uart.readline()
                    self.K_p = float(self.K_p)
                    
                if(self.K_p != 0):
                    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                    pinA5.high ()
                    #Checking to see if there are still vlaues of step input to be read
                    if(self.step_count < 50):
                        #Sending over The Step value for Omega ref
                        #print(self.step_count)
                        self.Sender = 0
                        shares.cmd1 = self.Sender
                        shares.cmd  = self.K_p
                        self.step_count += 1
                        self.transitionTo(self.S1_PASSING_DATA)
                    
                    elif(self.step_count > 150):
                        self.Sender = str('Stop')
                        shares.cmd1 = self.Sender
                        shares.cmd  = self.K_p
                        self.step_count += 1
                        shares.cmd = None
                        shares.cmd1 = None
                        self.transitionTo(self.S2_SENDING_DATA)
                        
                    elif(self.step_count >= 50):
                        self.Sender = 90
                        shares.cmd1 = self.Sender
                        shares.cmd  = self.K_p
                        self.step_count += 1
                        self.transitionTo(self.S1_PASSING_DATA)
                        # Checking to see if shares.resp has an omega stored
                        
                    if(shares.resp != None):
                            
                            # Appending Time and Velocity Lists
                        self.time.append(self.next_time)
                        self.Velocity.append(shares.resp)
                            
                        shares.resp = None
                            # rerunning the loop
                        self.transitionTo(self.S1_PASSING_DATA)
                            #Resets the loop if it misses a data point    
                        
                    # When there are no more entries to be read from the step input
                else:
                    self.transitionTo(self.S1_PASSING_DATA)
               

             # state where data is sent from the Nucleo back to the PC via REPL
             elif(self.state == self.S2_SENDING_DATA):
                 #For loop that prints the length of the lists in CSV format
                 
                 for n in range (len(self.Velocity)):
                     print('{:},{:}'.format(self.time[n],self.Velocity[n]))
                 print('END')
                 self.transitionTo(self.S3_NULL)
                    
                    
                
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
             self.runs += 1
             
             self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

     
        
    

