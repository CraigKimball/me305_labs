'''
@file main_lab06.py

@brief This is the main task program for running the Encoder and UI cooperatively

This program tracks the total elapsed position of an encoder 
and will print information about the encoder to the user when prompted. The interval for this task
must be quick enough such that the 16 bit encoder does not travel through and over its range in one iteration of the FSM
Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab04/main.py

'''

import pyb
# Importing finite state machine tasks
from Lab06 import ControllerTask
from UI_back_end import BackEnd
# importing drivers
from encoder import Encoder
from Motor_Driver import MotorDriver
from ClosedLoop_Class import ClosedLoop

## Controller Object
Controller = ClosedLoop()
## Motor Object

nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
IN1_pin = pyb.Pin(pyb.Pin.cpu.B4)
IN2_pin  = pyb.Pin(pyb.Pin.cpu.B5)
timer = pyb.Timer(3,freq = 20000)

Motor = MotorDriver(nSLEEP_pin, IN1_pin, IN2_pin, timer)
## Encoder Object
encoder_track = Encoder()

## Task Objects
task1 = ControllerTask(.1, Controller, encoder_track, Motor)
task2 = BackEnd(.1)


# To run the task call task1.run() over and over
while True: #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
