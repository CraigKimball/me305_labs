'''
@file main_lab06.py
@image html Lab06_control_layout.png width=800cm
@brief This is the main task program for running the UI backend and controller task for the motor 
controller lab cooperatively

This program controls the function of two FSM that work inside our controller diagram in order
to contro angular motor velocity. IT controls the UI backend which takes data from the user and sends it
to the controller to calculate error. The controller takes the error between desired and measured velocity and caluclates
a PWM % necessary to fix the error and applies it to the motor. The UI backend also saves measurements taken of the measured velocity
at each interval to be sent back to the front end for plotting a step response.

For the first run of the motor a Kp value of 0.45 was chosen with a set point of 900 deg/sec
the following plot was generated

@image html Vel_Time_045.png width=800cm

@details as you can see this gain value was not high enough for us to achieve the desired step response. There are also two
spikes in the measured motor speed which I believe to be extraneous data points due to a caluclation error by the motor controller.
Next the motor was fed a new Kp value of 0.55 and the following plot was generated.

@image html Vel_Time_055.png width=800cm

@details Here we can see that we are getting closer to the desired 900 deg/sec response. However the motor was still following short
only reaching about 7600rpm. There was also another point calculation eror here as seen by the large spike in the graph. I am attributing these errors to a missed datapoint
by the controller causing a 0 value and the motor hitting the programmed satuartion limit for acuation value. Finally one more trial was performed tuning
the Kp to 0.7

@image html Vel_Time_070.png width=800cm

@details As you can see the motor still falls ahort of the desired response which is partly due to the fact that a propotional controller, is a
very weak controller on its own and the motor has some built in friction in the gear train making it unideal. To imporve this reponse I would 
probably add an Integral controller to help. Links to the images have also been provided below

Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/main_lab06.py
Images: Kp = .45: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/Vel_Time_045.png
Images: Kp = .55: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/Vel_Time_055.png
Images: Kp = .7:  https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab06/Vel_Time_070.png

'''

import pyb
# Importing finite state machine tasks
from Lab06 import ControllerTask
from UI_back_end import BackEnd
# importing drivers
from encoder import Encoder
from Motor_Driver import MotorDriver
from ClosedLoop_Class import ClosedLoop

## Controller Object
Controller = ClosedLoop()
## Motor Objects
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
IN1_pin = pyb.Pin(pyb.Pin.cpu.B0)
IN2_pin  = pyb.Pin(pyb.Pin.cpu.B1)
timer = pyb.Timer(3,freq = 20000)

Motor = MotorDriver(nSLEEP_pin, IN1_pin, IN2_pin, timer)
## Encoder Object
encoder_track = Encoder()

## Task Objects
task1 = ControllerTask(.01, Controller, encoder_track, Motor)
task2 = BackEnd(.01)


# To run the tasks cooperatively
while True: 
    task1.run()
    task2.run()
