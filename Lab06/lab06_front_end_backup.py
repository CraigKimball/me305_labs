'''
@file User_interface_lab04.py

@brief This file is a FSM used to hadle PC user interface

@detials This class implements a finite state machine to control the
user inputs and output plots
Source Link:https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab04/User_interface_lab04.py
'''
import numpy as np
import matplotlib.pyplot as plt
import time
import serial
import keyboard
class TaskUI:
    '''
    @brief      A finite state machine To handle User Input and plot encoder movement
    @details    This class implements a finite state machine to control the
                user inputs and output plots
    '''
    
    ## Constant defining State 0 
    S0_INIT                         = 0
    
    ## Constant defining State 1
    S1_CHECK_INPUT_STATE       = 1
    
    ## Constant defining State 2
    S2_OUTPUT       = 2
    ## Constant defining State 3
    S3_PLOT         = 3
    ## Constant defining State 4
    # This state is the end of the program. Nothing occurs here until user resets program
    S4_END          = 4
    
 
    
    def __init__(self, interval):
        '''
        @brief      Creates a UI object.
   
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## A class attribute

        # Creates empty arrays to store the printed Encoder pos and time from board
        self.Velocity = []
        self.Time       = []
        
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
        self.Timer10 = time.time()
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteration of the Encoder checking its own value
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Initializing')
                print('press g to begin collecting data. Press s to cancel or wait ten seconds')
                # use a serial command here to clear the buffer
                self.transitionTo(self.S1_CHECK_INPUT_STATE)
                #print(str(self.runs) + ': State 0 ' + str(utime_ticks_diff(self.curr_time,self.start_time)))
                
            
            
             ## State where User is asked for an input. used for start and stop
             # g will start the recording and s will stop the recording and plot
             elif(self.state == self.S1_CHECK_INPUT_STATE):
                # Asking User for an input
                
                # if the user presses g
                if(keyboard.is_pressed('g')):
                    self.K_p = input('Please enter a number to the tenth for Kp.')
                    print('input received begin data collection press S to stop')
                    
                    #Multuplying by ten to make a whole number to send as a bytearray
                    self.K_p = self.K_P * 10
                    
                    # sending data as a byte array
                    self.ser.write(self.K_p)
                    self.transitionTo(self.S1_CHECK_INPUT_STATE)
                # if the user presses s
                elif(keyboard.is_pressed('s')):
                    print('ending data collection')
                    # this will send specifically an 's' to nucleo if s or 10 sec happens
                    self.ser.write(str('s').encode('ascii'))
                    self.transitionTo(self.S2_OUTPUT)
                else:
                    self.transitionTo(self.S1_CHECK_INPUT_STATE)
             
             # State where data is taken from Nucleo and stored in local array
             elif(self.state == self.S2_OUTPUT):
                 
                 #creates a variable that holds the raw REPL output from nucleo with /n
                 self.raw_data = self.ser.readline().decode('ascii')
                 #Takes raw data and removes the /n from the string
                 converted = self.raw_data.strip()
                 if(converted != 'END'):
                 #creates a temporary list that holds the time and Encoder pos value split from string into a list
                     temp_list = converted.split(',')
                     
                 # appends temporary list values to local lists as floating point 
                     self.EncoderPos.append(float(temp_list[1]))
                     self.Time.append(float(temp_list[0])/1e6)
                     self.transitionTo(self.S2_OUTPUT)
                 elif(converted == 'END'):
                     self.ser.close()
                     self.transitionTo(self.S3_PLOT)
                     
             elif(self.state == self.S3_PLOT):
                 #Making plotted lists into exportable array
                 data1 = np.array(self.EncoderPos)
                 data2 = np.array(self.Time)
                 #saving array as a CSV file
                 np.savetxt('Encoder_Pos.csv',data1,delimiter=',')
                 np.savetxt('Time.csv',data2,delimiter=',')
                 
                 #Plotting the Encoder Position and Time Lists
                 plt.plot(self.Time,self.EncoderPos)
                 plt.xlabel('Time (sec)')
                 plt.ylabel('Encoder Position')
                 plt.show()
                 self.transitionTo(self.S4_END)
                 

                 
                 
                 
                 
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass

             self.runs += 1
             self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

        
    

