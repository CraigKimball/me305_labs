# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 07:32:04 2020

@author: craig
"""
import numpy
from numpy import genfromtxt
import array
import time
import serial
import test_shares


timing     = array.array('f',[])
position = array.array('f',[])
velocity = array.array('f',[])



with open('reference.csv', 'r') as ref:
    read_count = 0
    for line in ref:
        read_count += 1
        if read_count % 10 == 0:
            (t,v,x) = line.strip().split(',');
            timing.append(float(t))
            velocity.append(float(v))
            position.append(float(x))
            
print(len(velocity))
n = 0
while n < len(velocity):
    print(velocity[n])
    time.sleep(.01)
    n += 1