'''
@file lab07_front_end.py

@brief This module imports data from a CSV file and asks for a KP input. Once given it begins
passing CSV data to the motor controller. Once finished it calculates error and plots velocity and position

@details This module opens the reference CSV file, but instead of importing all 15000 rows only
imports every 10 rows. The reason this was done was to obtain sufficent resolution on the plots while
keeping used memory on the nucleo at a minimum. Once imported it asks the user to set a Kp value. Once given
data is then passed to the nucleo.

The way the data is passed was setup specifically to keep memory usage on the Nucleo minimal. This is acomplished by using a loop
that sends one row of data a t atime and then waits an interval before setting another row. This means there is only one row of 
sent data on the Nucleo at a time. 

Once data has been sent it begins receiving the stored position and velocity array on the Nucleo and then uses these values along with the CSV 
values to calculate the error. After error is caluclated plots are made to superimpose the actual motor velocity and position profile
agains the CSV data.

@image html Lab07_plot.png width=800cm

@details This plot shows the velocity and position profile of the measured data agianst the reference
data from the CSV file. From the plots we can see the measured posiiton data follows the reference data fairly closely
indicating the K_p value is likely appropriate for this system. The error calculated on this run was: 1951.7

An important thing to note is that When the velocity is stored It is multiplied by 37.5. For some reason when the velocity
array from the motor was returned all the data entries were off by a factor of 37.5 Scanning through my code I was unable to determine what was causing this 
issue with the time remaining, but the position data being close to the reference makes me think that it is not a unit conversion issue, but something going on when the data is being
passed around. To fix this I just added a multiplier on the front end

Source Link:https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab07/Lab07.py
'''

# Importing needed libraries
import numpy as np
from numpy import genfromtxt
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import time
import serial
import array

##List for plotting that will contain measured velocity data
Velocity = []
## List for plotting that will contain measured Position data
position_measured = []

converted = None


## Opening Serial connection
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

## Array that stores the Time from the Reference file
timing     = array.array('f',[])
## Array that stores the position column from the Reference file
position = array.array('f',[])
## Array that stores the velocity column from the Reference file
velocity = array.array('f',[])


## Using a With command to open the CSV. A counter is defined and then
# a for loop is used that incements down the rows of the CSV.
with open('reference.csv', 'r') as ref:
    read_count = 0
    for line in ref:
        read_count += 1
        # If the row # is divisible evenly by 100 with a remainder of 0
        if read_count % 100 == 0:
            #  Pulling data from the rows and assigning them to their own arrays
            (t,v,x) = line.strip().split(',');
            timing.append(float(t))
            velocity.append(float(v))
            position.append(float(x))

## Asking the user for a input Kp value for the controller.
# This value will be the proporitonal gain passed to the controller
K_p = input('Please ener a proportional gain value follwoing the format: "X.X" ')
# Floating the value because strings cannot be sent across the serial port
K_p = float(K_p)
# Writing K_p to the Nucleo
ser.write('{:}\r\n'.format(K_p).encode())

time.sleep(2)
# Counter for sending the 1500 data points of velocity
n = 0
# While data is setill left to be sent in velcoity array
while n < len(velocity):
    #Sending a single Omega reference data point
    ser.write('{:}\r\n'.format(velocity[n]).encode())
    print(velocity[n])
    #print(velocity[n])
    # Waiting the FSM interval so that the FSM doesn't miss a data point
    time.sleep(0.15)
    n += 1



# Sleep the program for 5 seconds. This is longer than the time 
# it will take  for the program to make the reponse arrays

time.sleep(10)

 
 #creates a variable that holds the raw REPL output from nucleo with /n


## Creates plotting arrays until END message is read
# Converted is the holder variabkle for formatting received data to be stored in local lists
while(converted != 'END'):
    raw_data = ser.readline().decode('ascii')
 #Takes raw data and removes the /n from the string
    converted = raw_data.strip()
 #creates a temporary list that holds the time and Encoder pos value split from string into a list
    temp_list = converted.split(',')
    
 # appends temporary list values to local lists as floating point 
    if(temp_list[0] != 'END'):
        print(temp_list)
        Velocity.append(float(temp_list[1]) * 37.5)
        position_measured.append(float(temp_list[0]))

# Creating variables for calculating error
J = 0
k = 0

## deleting one entry from the velocity and position arrays 
# the reason for this is that the returned measured velocity and position lists only
#have 149 entries due to the logic on the backend.
del velocity[-1]
del position[-1]
del timing[-1]
## Using a loop to calculate error in the system
while k < len(velocity):
    ## The equation used to calculate error of the data compared to the CSV data in each row
    J_add = ((velocity[k] - Velocity[k])**2) + ((position[k] - position_measured[k])**2)
    J = J + J_add
    k += 1
#Caluclating total error
error = (1/150) * J
print(error)



## Creating subplots for Position and Velocity
# The measured data will be overlaid against the CSV data

# First sub plot
plt.subplot(2, 1, 1)

plt.plot(timing,Velocity, 'r',timing,velocity)
plt.xlabel('Time (sec)')
plt.ylabel('Velocity (RPM)')
plt.title( 'Velcoity Profile')
red_patch = mpatches.Patch(color='red', label='Measured Values')
blue_patch = mpatches.Patch(color='blue', label='Reference Data')
plt.legend(handles=[red_patch,blue_patch])
#Second sub plot
plt.subplot(2, 1, 2)
plt.plot(timing,position_measured,'r',timing,position)
plt.xlabel('Time (sec)')
plt.ylabel('Position (deg)')
plt.title( 'Motor Position')
red_patch = mpatches.Patch(color='red', label='Measured Values')
blue_patch = mpatches.Patch(color='blue', label='Reference Data')
plt.legend(handles=[red_patch,blue_patch])
plt.show()

ser.close()


    

