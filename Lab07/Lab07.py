# -*- coding: utf-8 -*-
"""
@file Lab07.py

@brief This module implements use of the Encoder driver, motor driver, and closed loop controller to control a motor
@details This task is passed an Omega_ref from the UI back end and a K_p value. Once received
it first pulls the current encoder position and compares it to the last know n position to get measured velocity.
Then it uses this data to calculate the PWM acuation level needed to correct the Omega error and sends that 
value to the motor to adjust. This program will iterate as long as step_input data is continues to be sent.
Source Code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab07/Lab07.py
"""

## Importing necessary classes for controlling motor controller
import pyb
import utime
import shares
from ClosedLoop_Class import ClosedLoop
from encoder import Encoder
from Motor_Driver import MotorDriver

class ControllerTask:
    '''
    @brief This Class uses Encoder, motor, and Controller classes to calculate Omega error and adjust motor power
    @details  This class utilizes The Encoder Class, Controller Class, and then motor class in order to first; calculate the velocity
    of the encoder. Once found it grabs Omega ref and Kp to calculate the new needed acuation level for PWM on the motor. This data is then sent
    to the motor and the measured omega is stored and sent back to the UI backend
    '''
## Constant defining State 0: Initialization State
    S0_INIT                         = 0
    
    ## Constant defining State 1: Calculating New motor Acuation value
    S1_CALCULATIONS                 = 1


    """This class implements the encoder and motor driver to control a motor and track velocity"""
    
    def __init__(self, interval, Controller, EncoderTrack, Motor):
        '''
        @brief      Creates a LED object.
        @param Controller an object from class Closed Loop controller that calculates PWM percent level
        @param EncoderTrack an object from clas encoder that gives the change in encoder position allowing use to calculate velocity
        @param Motor an object from a motor class that accepts new acuation level and adjusts motor speed
        
        '''
        # Setting Up objects copies for Task to interact with
        self.EncoderTrack = EncoderTrack
        self.Controller = Controller 
        self.Motor  = Motor
        ## Setting up Pin objects for motor
        
        self.state = self.S0_INIT
        ## A class attribute copy for the LED
        ## A counter showing the number of times the task has run
        self.runs = 0
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    def run(self):
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                 self.Omega_ref = 0
                 self.K_p = 0
                 ## Zeroing The encoder and running Motor startup. Refer to 
                 # Motor_Driver.py module for documentation on what "enable" does
                 self.EncoderTrack.zero()
                 self.Motor.enable()
                 self.transitionTo(self.S1_CALCULATIONS)
                 
             elif(self.state == self.S1_CALCULATIONS): 
                 self.EncoderTrack.update()
                 # Storing current position of the output shaft in degrees
                 self.position = self.EncoderTrack.update()
                 # Looking to see if a Omega ref is being sent by backend task
                 if(shares.cmd1 != None):
                     # Pulling the sent variables through shares from the backend
                         
                        self.Omega_ref = shares.cmd1
                        self.K_p = shares.cmd
                        self.Omega_ref = float(self.Omega_ref)
                        self.K_p = float(self.K_p)
                        # Calculating the Measured Velocity of the motor in rev/min
                        self.Omega_measured = (self.EncoderTrack.get_delta() / .0025)
                        
                        # Using Controller class to calculate the PWM level as a %
                        self.New_duty = self.Controller.LoopRun(self.Omega_ref,self.K_p, self.Omega_measured)
                     
                        # Using the PWM signal generated to update voltage supplied to motor
                        self.Motor.set_duty(self.New_duty)
                     
                        # Passing Info back to UI backend to store in array
                        shares.resp = self.Omega_measured
                        #Converting measured position to degrees
                        shares.resp1 = self.position * (-11.11)
                     
                        # Clearing global variable
                        shares.cmd1 = None
                        shares.cmd = None
                        self.transitionTo(self.S1_CALCULATIONS)
                 else:
                     self.transitionTo(self.S1_CALCULATIONS)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState