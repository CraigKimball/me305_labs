'''
@file main_lab07.py
@image html Lab06_control_layout.png width=800cm
@brief This is the main task program for running the UI backend and controller task for the motor 
controller lab cooperatively

This program controls the function of two FSM that work inside our controller diagram in order
to contro angular motor velocity. IT controls the UI backend which takes data from the user and sends it
to the controller to calculate error. The controller takes the error between desired and measured velocity and caluclates
a PWM % necessary to fix the error and applies it to the motor. The UI backend also saves measurements taken of the measured velocity
at each interval to be sent back to the front end for plotting a step response.
Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab07/main_lab07.py

'''

import pyb
# Importing finite state machine tasks
from Lab07 import ControllerTask
from UI_back_end_lab07_alternate import BackEnd
# importing drivers
from encoder import Encoder
from Motor_Driver import MotorDriver
from ClosedLoop_Class import ClosedLoop

## Controller Object
Controller = ClosedLoop()
## Motor Objects
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)
IN1_pin = pyb.Pin(pyb.Pin.cpu.B0)
IN2_pin  = pyb.Pin(pyb.Pin.cpu.B1)
timer = pyb.Timer(3,freq = 20000)

Motor = MotorDriver(nSLEEP_pin, IN1_pin, IN2_pin, timer)
## Encoder Object
encoder_track = Encoder()

## Task Objects
task1 = ControllerTask(.15, Controller, encoder_track, Motor)
task2 = BackEnd(.15)


# To run the tasks cooperatively
while True: 
    task1.run()
    task2.run()
