# -*- coding: utf-8 -*-
"""
@file Serial_com_example.py

@author: craig
"""

import serial
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range (1):
    print(sendChar())

ser.close()