'''
@file main_hw0x0.py
@image html hw_0_FSM.png width=1000cm
@brief This is the main task program for running the elevator FSM

This program allows for use to operate multiple instances of the FSM. In the
 current state it is running to instances of the elevator
at the same time. Source code:https://bitbucket.org/CraigKimball/me305_labs/src/master/Homework/main_hw0x0.py

'''

## @image html hw_0_FSM.png

from Hw0x0 import Button, MotorDriver, TaskElevator
        
## Motor Object
Motor = MotorDriver()
Button_1 = Button('PB6')
Button_2 = Button('PB7')
First = Button('PB8')
Second = Button('PB9')

## Task object
task1 = TaskElevator(0.1, Button_1, Button_2, First, Second, Motor) # Will also run constructor
task2 = TaskElevator(0.1, Button_1, Button_2, First, Second, Motor) # Will also run constructor
# To run the task call task1.run() over and over
for N in range(100000000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()
