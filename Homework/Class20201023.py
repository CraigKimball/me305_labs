# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 10:15:38 2020

@author: craig
"""

import matplotlib.pyplot as plt
import numpy as np



def clearlists():
    
    time.clear()
    x.clear()
    xdot.clear()
    x2dot.clear()
    
    
def ODE(x0):
    time.append(0)
    x.append(x0)
    xdot.append(0)
    x2dot.append(-k*x[0]-a*x[0]**3)
    
    for i in range(1,1000):
        time.append(time[i-1]+delt)
        x.append(x[i-1]+delt*xdot[i-1]+0.5*delt**2*x2dot[i-1])
        x2dot.append(-k*x[i-1]-a*x[i-1]**3)
        xdot.append(xdot[i-1]+0.5*delt*(x2dot[i-1]+x2dot[i]))

k = 2 
a = 5
delt = 0.01

time = []
x = []
xdot = []
x2dot = []

for x0 in [0.2,0.4,0.6,0.8,1.0]:
    clearlists()
    ODE(x0)
    plt.plot(time,x)

plt.show()
    
    
    
    



    
