# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 14:07:16 2020

@author: craig
"""

import numpy as np
import matplotlib.pyplot as plt
import time
import serial
import keyboard

time = time.time()
EncoderPos = []
Time = []
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


inv = input('press g to begin data colletion. Will stop after ten seconds or s is pressed')
ser.write(str(inv).encode('ascii'))
print('input received begin data collection')
while(inv == 'g'):

    if(keyboard.is_pressed('s')):
        ser.write(str('s').encode('ascii'))
        print('ending collection')
        inv = 's'
raw_data = ser.readline().decode('ascii')
        #Takes raw data and removes the /n from the string
converted = raw_data.strip()
        #creates a temporary list that holds the time and Encoder pos value split from string into a list
temp_list = converted.split(',')
        # appends temporary list values to local lists as floating point #
for n in range (100):
    EncoderPos.append(float(temp_list[1]))
    print(Time)
    Time.append(float(temp_list[0])/1e6)
    plt.plot(Time,EncoderPos)
    n +=1
plt.show()
ser.close()

    
                   
