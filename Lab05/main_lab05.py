'''
@file main_lab05.py
@image html Lab05_com.png width=800cm
@brief This is the main task program for running our LED blinking FSM

This program runs the FSM on the Nucleo board for blinking a LED at a user set frequency.
Specifically in this program we set the LED object in the FSM to understand our on off class
and we set the interval for the FSM to run. It is important that we choose a interval that is very small
so that we can accurately blink the LED at the desired frequency and, so there is as little delay as possible
between setting a new frequency and LED beginning to blink at the desired frequency.
Source code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab05/main_lab05.py
App Source Code https://x.thunkable.com/projects/5fa5c52d17c64300118297b8/dd24d78d-df2f-440e-a6ad-924c886146b1/designer

'''



from Lab05 import TaskLED, LED_State

## LED object
LED = LED_State()

##Here we defined the interval at .01 seconds. It is important this matches the interval of our F_count variable in the FSM
task1 = TaskLED(0.01, LED)


# To run the task call task1.run() over and over
while True: 
    task1.run()
#    task2.run()
#    task3.run()
