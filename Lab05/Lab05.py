'''
@file Lab05.py

@brief This file is a FSM used to run a blinking LED with a frequency controlled over bluetooth
@image html Lab05_FSM.png width=1000cm
@detials This FSM looks for input over a BLE connection. The user,
using a UI on their Smart Device will set a desired LED frequency and the phone
will send the signal over bluetooth to the LED to start blinking at a desired frequency
Source Code: https://bitbucket.org/CraigKimball/me305_labs/src/master/Lab05/Lab05.py
'''

import pyb
import utime
from pyb import UART
class TaskLED:
    '''
    @brief      A finite state machine to control a Blinking LED
    @details    This class implements a finite state machine to control the
                blinking rate of the LED and handle any inputs from the user
    '''
    
    ## Constant defining State 0: Initialization State
    S0_INIT                         = 0
    
    ## Constant defining State 1: Setting LED blink rate to User Input
    S1_SETTING_TIME                 = 1
    
    ## Constant defining State 2: Blinking the LED at the set frequency
    S2_BLINK                     = 2
    
 
    
    def __init__(self, interval, LED):
        '''
        @brief      Creates a LED object.
        @param LED an object from class LED representing THE Physical and virtual LED
        
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## A class attribute copy for the LED
        self.LED = LED
        
        # Setting the LED to start off
        self.LED_val = 0
        
        # Setting up the UART connection that the bluetooth antenna will use to speak with nucleo
        self.uart = UART(3, 9600)
       

        
        ## A counter showing the number of times the task has run
        self.runs = 0
        ## A counter that specifically tracks runs of state 2 to set blink frequency
        self.F_count = 0
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since hardware turned on
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((interval*1e6))       
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief      Runs one iteration of the task 
        @details    Runs one iteratoin of Time passing and the LED operating
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                # Sets Frequency to interval (.01)
                # Zeroing all user input variables and counts
                self.Input_Time = 0
                self.Corrected_Time = 0
                self.F_count = 0
                self.transitionTo(self.S1_SETTING_TIME)
                
                
             elif(self.state == self.S1_SETTING_TIME):
                # Checking to see if the user has sent information to the Nucleo
                if(self.uart.any() != 0):
                    ## Sets the input time to the sent Time from UI and converts to micro sec
                    self.Input_Time =  int(self.uart.readline())
                    ## Frequency is 1/time so the corrected time gives the interval at which the LED will blink
                    self.Corrected_Time = (1/self.Input_Time)
                    self.F_count = 0
                    # Sends a message to user to indicate what frequency LED blinks at
                    print('LED is Blinking at' + str( self.Input_Time) + 'Hz')
                    self.transitionTo(self.S2_BLINK)   
                        
                else:
                    self.transitionTo(self.S2_BLINK)
                
               
             elif(self.state == self.S2_BLINK):
              
               # using a count on the FSM to see if set blinking interval has elapsed. Int = 0.01
               if(self.F_count >= self.Corrected_Time):
                 
                    # Checking the Current State of the LED
                    if(self.LED_val == 1):
                        self.LED.off()
                        self.LED_val = 0
                        self.F_count = 0
                        
                    elif(self.LED_val == 0):
                        self.LED.on()
                        self.LED_val = 1
                        self.F_count = 0
                        
                    self.transitionTo(self.S2_BLINK)
               # Sends FSM back to state one to change frequency if it detects user input 
               if(self.uart.any() != 0):
                    self.transitionTo(self.S1_SETTING_TIME)
                
            
                
            
             else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
             self.F_count += 0.01
             self.runs += 1
             self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
     
        
    
 


class LED_State:
    '''
    @brief      A LED_State Class that turns the LED on and off
    @details    This class allows us to efficently turn the LED on or off in the FSM
                
    '''
    
    def __init__(self):
        
        '''
    
        @brief Creates a LED Object
        @details Here we are setting up pinA5 (our LED on the Nucleo) to be able to turn
        on and off.
        '''
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    
    
  
    def on(self):
        '''
        @brief Turns LED on
        '''
        print('LED ON')
        self.pinA5.high()
        
    
    
    def off(self):
        '''
        @brief Turns LED off
        '''
        print('LED OFF')
        self.pinA5.low()
        
        
    

