# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 12:55:18 2020

@author: craig
"""
from pyb import UART
import pyb

uart = UART(3, 9600)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
val = 0

while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val,'Turns it OFF')
            pinA5.low()
        elif val ==1:
            print(val,' turns it ON')
            pinA5.high()